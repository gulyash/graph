#include <string>
#include <fstream>
#include <sstream>
#include <vector>
#include <set>
#include <tuple>
#include <map>
#include <algorithm>
#include <cstdlib>
#include <stack>				
#include <queue>

using namespace std;


//сравнение веса двух ребер
bool compareKruscal(const tuple <int, int, int> &t1, const tuple <int, int, int> &t2)
{
	if (get<2>(t1) != get<2>(t2)) return (get<2>(t1) < get<2>(t2));
	else if (get<0>(t1) != get<0>(t2)) return (get<0>(t1) < get<0>(t2));
	else return (get<1>(t1) < get<1>(t2));
}


//disjoint set union - система непересекающихся множеств
class DSU {
private:
	vector <int> previous;
	vector <int> rank;
public:
	DSU(int n) {
		previous.resize(n + 1);
		rank.resize(n + 1);
		for (int i = 1; i <= n; i++) {
			previous[i] = i;
			rank[i] = 1;
		}
	}

	int getPrevious(int i) {
		return previous[i];
	}

	int find(int x) {
		if (x == previous[x]) {
			return x;
		}
		return previous[x] = find(previous[x]); //Path compressing
	}

	void unite(int x, int y) {
		x = find(x);
		y = find(y);
		if (x != y) {
			if (rank[x] < rank[y]) swap(x, y); //Rank matters, rank = tree size, not tree depth
			previous[y] = x;
			rank[x] += rank[y];
		}
	}

	bool checkConnectivity(){
		for (unsigned int i = 1; i < previous.size() - 1; i++) {
			if (find(previous[i]) != find(previous[i + 1])) {
				return false;
			}
		}
		return true;
	}
};


//лаб1
class Graph {
private:
	int vertsAmount = 0, edgesAmount = 0;
	char graphType = 'C';
	int isOriented = 0, isWeighed = 0;
	vector<vector<int>>  adjMatrix;
	vector<map<int, int>> adjList;
	vector<tuple<int, int, int>>  listOfEdges;

	//установление типа представления графа
	void setStates(char graphTypeTmp, int isOrientedTmp, int isWeighedTmp, int vertsAmountTmp, int edgesAmountTmp)
	{
		graphType = graphTypeTmp;
		isOriented = isOrientedTmp;
		isWeighed = isWeighedTmp;
		vertsAmount = vertsAmountTmp;
		edgesAmount = edgesAmountTmp;
		switch (graphType) {
		case 'L': {		//список смежных вершин
					  adjList.resize(vertsAmount);
		}
			break;
		case 'C': {		//матрица смежности
					  adjMatrix.resize(vertsAmount);
					  for (int i = 0; i < vertsAmount; i++) {
						  adjMatrix[i].resize(vertsAmount);
					  }
		}
			break;
		case 'E': {		//список ребер
					  listOfEdges.resize(edgesAmount);
		}
			break;
		}
	}

public:
	Graph(){}

	void readGraph(string fileName) {
		adjList.clear();
		adjMatrix.clear();
		listOfEdges.clear();

		ifstream input;
		input.open(fileName);
		input >> graphType;

		switch (graphType) {
		case 'C':
			input >> vertsAmount >> isOriented >> isWeighed;

			adjMatrix.resize(vertsAmount);
			for (int i = 0; i < vertsAmount; i++)
				adjMatrix[i].resize(vertsAmount);

			for (int i = 0; i < vertsAmount; i++) {
				for (int j = 0; j < vertsAmount; j++) {
					input >> adjMatrix[i][j];
					if (adjMatrix[i][j] != 0) {
						edgesAmount++;
					}
					if (!isOriented) adjMatrix[j][i] = adjMatrix[i][j];
				}
			}
			if (!isOriented) edgesAmount /= 2;
			break;

		case 'L':
			input >> vertsAmount >> isOriented >> isWeighed;
			adjList.resize(vertsAmount);
			for (int i = 0; i < vertsAmount; i++) {
				string str;
				getline(input, str);
				istringstream lineStream(str);
				int adjverticeNum, weight = 1;
				while (lineStream && str.size() != 0) {
					lineStream >> adjverticeNum;
					if (isWeighed) {
						lineStream >> weight;
					}
					adjList[i][adjverticeNum] = weight;
					if (!isOriented) adjList[adjverticeNum - 1][i + 1] = weight;
				}
				edgesAmount += adjList[i].size();
			}
			break;

		case 'E':
			input >> vertsAmount >> edgesAmount >> isOriented >> isWeighed;
			listOfEdges.resize(edgesAmount);
			for (int i = 0; i < edgesAmount; i++) {
				int firstvertice, secondvertice, weight = 1;
				if (isWeighed) {
					input >> firstvertice >> secondvertice >> weight;
				}
				else {
					input >> firstvertice >> secondvertice;
				}
				get<0>(listOfEdges[i]) = firstvertice;
				get<1>(listOfEdges[i]) = secondvertice;
				get<2>(listOfEdges[i]) = weight;
			}
			break;
		}
	}

	void addEdge(int from, int to, int weight) {
		if (!isOriented && from > to) {
			swap(from, to);
		}
		switch (graphType) {
		case 'C':
			from--;
			to--;
			edgesAmount++;
			if (isOriented) {
				adjMatrix[from][to] = weight;
			}
			else {
				adjMatrix[from][to] = weight;
				adjMatrix[to][from] = weight;
			}
			break;
		case 'L':
			edgesAmount++;
			if (isOriented)  {
				adjList[from - 1][to] = weight;
			}
			else {
				adjList[from - 1][to] = weight;
				adjList[to - 1][from] = weight;
			}
			break;
		case 'E':
			edgesAmount++;
			listOfEdges.resize(edgesAmount);
			get<0>(listOfEdges[edgesAmount - 1]) = from;
			get<1>(listOfEdges[edgesAmount - 1]) = to;
			get<2>(listOfEdges[edgesAmount - 1]) = weight;
			break;
		}
	}


	void removeEdge(int from, int to) {
		switch (graphType) {
		case 'C':
			from--;
			to--;
			if (adjMatrix[from][to] != 0) {
				if (isOriented)  {
					adjMatrix[from][to] = 0;
				}
				else {
					adjMatrix[from][to] = 0;
					adjMatrix[to][from] = 0;
				}
				edgesAmount--;
			}
			break;
		case 'L': {
					  auto result1 = adjList[from - 1].find(to);
					  auto result2 = adjList[to - 1].find(from);
					  if (result1 != adjList[from - 1].end() && result2 != adjList[to - 1].end()) {
						  adjList[from - 1].erase(result1);
						  adjList[to - 1].erase(result2);
						  edgesAmount--;
					  }
					  else if (result2 != adjList[to - 1].end()) {
						  adjList[to - 1].erase(result2);
						  edgesAmount--;
					  }
					  else if (result1 != adjList[from - 1].end()) {
						  adjList[from - 1].erase(result1);
						  edgesAmount--;
					  }
					  break;
		}
		case 'E':
			for (auto vertice = listOfEdges.begin(); vertice != listOfEdges.end(); vertice++) {
				if (isOriented) {
					if (get<0>(*vertice) == from && get<1>(*vertice) == to) {
						listOfEdges.erase(vertice);
						break;
					}
				}
				else {
					if (get<0>(*vertice) == from && get<1>(*vertice) == to || get<0>(*vertice) == to && get<1>(*vertice) == from) {
						listOfEdges.erase(vertice);
						break;
					}
				}
			}
			edgesAmount--;
			break;
		}
	}

	int changeEdge(int from, int to, int newWeight) {
		int oldValue = 0;
		switch (graphType) {
		case 'L': {
					  const auto result1 = adjList[from - 1].find(to);
					  const auto result2 = adjList[to - 1].find(from);
					  if (result1 != adjList[from - 1].end() && result2 != adjList[to - 1].end()) {
						  oldValue = result1->second;
						  result1->second = newWeight;
						  result2->second = newWeight;
					  }
					  else if (result1 != adjList[from - 1].end()) {
						  oldValue = result1->second;
						  result1->second = newWeight;
					  }
					  if (result2 != adjList[to - 1].end()) {
						  oldValue = result2->second;
						  result2->second = newWeight;
					  }
					  return oldValue;
					  break;
		}
		case 'C':
			from--;
			to--;
			oldValue = adjMatrix[from][to];
			adjMatrix[from][to] = newWeight;
			if (!isOriented) adjMatrix[to][from] = newWeight;
			break;

		case 'E':
			for (auto it = listOfEdges.begin(); it != listOfEdges.end(); it++) {
				if (isOriented) {
					if (get<0>(*it) == from && get<1>(*it) == to) {
						oldValue = get<2>(*it);
						get<2>(*it) = newWeight;
						break;
					}
				}
				else if (get<0>(*it) == from && get<1>(*it) == to || get<0>(*it) == to && get<1>(*it) == from) {
					oldValue = get<2>(*it);
					get<2>(*it) = newWeight;
					break;
				}
			}
			break;
		}
		return oldValue;
	}


	//преобразовать в список смежности
	void transformToAdjList() {
		switch (graphType) {
		case 'C':
			adjList.resize(vertsAmount);
			for (int i = 0; i < vertsAmount; i++) {
				for (int j = i; j < vertsAmount; j++) {
					if (adjMatrix[i][j] != 0) adjList[i][j + 1] = adjMatrix[i][j];
				}
			}
			adjMatrix.clear();
			adjMatrix.shrink_to_fit();
			graphType = 'L';
			break;
		case 'E':
			adjList.resize(vertsAmount);
			for (int i = 0; i < edgesAmount; i++) {
				int ax = get<0>(listOfEdges[i]);
				int ay = get<1>(listOfEdges[i]);
				int aw = get<2>(listOfEdges[i]);
				adjList[ax - 1][ay] = aw;
				if (!isOriented) {
					adjList[ay - 1][ax] = aw;
				}
			}
			listOfEdges.clear();
			listOfEdges.shrink_to_fit();
			graphType = 'L';
			break;
		}
	}

	//преобразовать в матрицу смежности
	void transformToAdjMatrix() {
		switch (graphType) {
		case 'L':
			adjMatrix.resize(vertsAmount);
			for (int i = 0; i < vertsAmount; i++) {
				adjMatrix[i].resize(vertsAmount);
			}
			for (int i = 0; i < vertsAmount; i++) {
				for (auto it = adjList[i].begin(); it != adjList[i].end(); it++) {
					int ay = it->first;
					int aw = it->second;
					adjMatrix[i][ay - 1] = aw;
					if (!isOriented) adjMatrix[ay - 1][i] = aw;
				}
			}
			adjList.clear();
			adjList.shrink_to_fit();
			graphType = 'C';
			break;
		case 'E':
			adjMatrix.resize(vertsAmount);
			for (int i = 0; i < vertsAmount; i++) adjMatrix[i].resize(vertsAmount);
			for (int i = 0; i < edgesAmount; i++) {
				int ax = get<0>(listOfEdges[i]);
				int ay = get<1>(listOfEdges[i]);
				int aw = get<2>(listOfEdges[i]);
				adjMatrix[ax - 1][ay - 1] = aw;
				if (!isOriented) adjMatrix[ay - 1][ax - 1] = aw;
			}
			listOfEdges.clear();
			listOfEdges.shrink_to_fit();
			graphType = 'C';
			break;
		}
	}


	//проеобразовать в список ребер
	void transformTolistOfEdges() {
		int itk;
		switch (graphType) {
		case 'L':
			listOfEdges.resize(edgesAmount);
			itk = 0;
			for (int i = 0; i < vertsAmount; i++) {
				for (auto it = adjList[i].begin(); it != adjList[i].end(); it++) {
					int ay = it->first;
					int aw = it->second;
					get<0>(listOfEdges[itk]) = i + 1;
					get<1>(listOfEdges[itk]) = ay;
					get<2>(listOfEdges[itk]) = aw;
					itk++;
				}
			}
			adjList.clear();
			adjList.shrink_to_fit();
			graphType = 'E';
			break;
		case 'C':
			listOfEdges.resize(edgesAmount);
			itk = 0;
			for (int i = 0; i < vertsAmount; i++) {
				for (int j = i; j < vertsAmount; j++) {
					if (adjMatrix[i][j] != 0) {
						get<0>(listOfEdges[itk]) = i + 1;
						get<1>(listOfEdges[itk]) = j + 1;
						get<2>(listOfEdges[itk]) = adjMatrix[i][j];
						itk++;
					}
				}
			}
			adjMatrix.clear();
			adjMatrix.shrink_to_fit();
			graphType = 'E';
			break;

		}
	}

	void writeGraph(string fileName) {
		ofstream output;
		output.open(fileName);
		switch (graphType) {
		case 'L':
			output << graphType << " " << vertsAmount << "\n";
			output << isOriented << " " << isWeighed << "\n";
			for (int i = 0; i < vertsAmount; i++) {
				for (auto it = adjList[i].begin(); it != adjList[i].end(); it++) {
					if (isWeighed){
						output << it->first << " " << it->second << " ";
					}
					else {
						output << it->first << " ";
					}
				}
				output << "\n";
			}
			break;

		case 'C': //матрица смежности
			output << graphType << " " << vertsAmount << "\n";
			output << isOriented << " " << isWeighed << "\n";
			for (int i = 0; i < vertsAmount; i++) {
				for (int j = 0; j < vertsAmount; j++) {
					output << adjMatrix[i][j] << " ";
				}
				output << "\n";
			}
			break;
		case 'E':
			output << graphType << " " << vertsAmount << " " << edgesAmount << "\n";
			output << isOriented << " " << isWeighed << "\n";
			for (int i = 0; i < edgesAmount; i++) {
				if (isWeighed) {
					output << get<0>(listOfEdges[i]) << " " << get<1>(listOfEdges[i]) << " " << get<2>(listOfEdges[i]);
				}
				else {
					output << get<0>(listOfEdges[i]) << " " << get<1>(listOfEdges[i]);
				}
				output << "\n";
			}
			break;
		}
	}

	//лаб2
	//остовное дерево - алгоритм Краскала
	Graph getSpanningTreeKruskal()
	{
		Graph graph;
		graph.setStates('E', isOriented, isWeighed, vertsAmount, 0);
		transformTolistOfEdges();

		//сортировка ребер по весу
		sort(listOfEdges.begin(), listOfEdges.end(), compareKruscal);

		//система непересекающихся множеств
		DSU dsu(vertsAmount);

		for (int i = 0; i < edgesAmount; i++) {
			int a = get<0>(listOfEdges[i]);
			int b = get<1>(listOfEdges[i]);
			int w = get<2>(listOfEdges[i]);
			if (dsu.find(a) != dsu.find(b))
			{
				graph.addEdge(a, b, w);
				dsu.unite(a, b);
			}
		}
		return graph;
	}

	//остовное дерево алгоритм Борувки
	Graph getSpanningTreeBoruvka() {
		Graph graph;
		graph.setStates('E', isOriented, isWeighed, vertsAmount, 0);
		transformTolistOfEdges();
		DSU dsu(vertsAmount);
		int componentsNum = vertsAmount;
		while (componentsNum > 1) {

			bool flag = true;

			//cheap - вектор для хранения вершины, -1 => 
			vector <int> cheap(vertsAmount + 1, -1);
			for (int i = 0; i < edgesAmount; i++) {
				int a = dsu.find(get<0>(listOfEdges[i]));
				int b = dsu.find(get<1>(listOfEdges[i]));
				int w = get<2>(listOfEdges[i]);
				if (a != b) {
					if (cheap[a] == -1 || w < get<2>(listOfEdges[cheap[a]])) {
						cheap[a] = i;
						flag = false;
					}
					if (cheap[b] == -1 || w < get<2>(listOfEdges[cheap[b]])) {
						cheap[b] = i;
						flag = false;
					}
				}
			}

			if (flag) break;
			for (int i = 1; i <= vertsAmount; i++) {
				if (cheap[i] != -1) {
					int a = get<0>(listOfEdges[cheap[i]]);
					int b = get<1>(listOfEdges[cheap[i]]);
					int w = get<2>(listOfEdges[cheap[i]]);
					if (dsu.find(a) != dsu.find(b)) {
						graph.addEdge(a, b, w);
						dsu.unite(a, b);
						componentsNum--;
					}
				}
			}
		}
		return graph;
	}


	//прима
	Graph getSpanningTreePrima()
	{
		Graph graph;
		graph.setStates('E', isOriented, isWeighed, vertsAmount, 0);
		//второе число - максимальный вес ребра для алгоритма
		vector <int> minEdgeWeight(vertsAmount + 1, 1000000);
		vector <int> minEdgeVertTo(vertsAmount + 1, -1);
		set <int> used;
		set <pair<int, int>> queue;
		queue.insert(make_pair(0, 1));
		minEdgeWeight[1] = 0;
		transformToAdjList();
		for (int i = 1; i <= vertsAmount; i++) {
			used.insert(i);
		}
		for (int i = 0; i < vertsAmount; i++) {
			if (queue.empty()) {
				queue.insert(make_pair(0, *used.begin()));
			}
			int v = queue.begin()->second;
			queue.erase(queue.begin());
			for (auto j = adjList[v - 1].begin(); j != adjList[v - 1].end(); j++) {
				int to = j->first;
				int cost = j->second;
				if (cost < minEdgeWeight[to] && used.find(to) != used.end()) {
					queue.erase(make_pair(minEdgeWeight[to], to));
					minEdgeWeight[to] = cost;
					minEdgeVertTo[to] = v;
					queue.insert(make_pair(minEdgeWeight[to], to));
				}
			}
			used.erase(v);
			if (!queue.empty()) {
				graph.addEdge(minEdgeVertTo[queue.begin()->second], queue.begin()->second, queue.begin()->first);
			}
		}
		return graph;
	}

	//лаб3
	//проверка существования Эйлерова пути в графе
	int checkEuler(bool &circleExist) {
		DSU dsu(vertsAmount);
		bool dsuFlag = false;
		int ans = 1, oddCount = 0;
		vector <int> power(vertsAmount + 1, 0);
		transformTolistOfEdges();
		for (int i = 0; i < edgesAmount; i++) {
			power[get<0>(listOfEdges[i])]++;
			power[get<1>(listOfEdges[i])]++;
		}
		for (int i = 1; i <= vertsAmount; i++) {
			if (power[i] % 2 == 1) {
				oddCount++;
				ans = i;
			}
		}
		for (int i = 0; i < edgesAmount; i++) {
			int a = get<0>(listOfEdges[i]);
			int b = get<1>(listOfEdges[i]);
			int w = get<2>(listOfEdges[i]);
			if (dsu.find(a) != dsu.find(b)) {
				dsu.unite(a, b);
			}
		}
		dsuFlag = dsu.checkConnectivity();
		(oddCount == 0)?circleExist = true:circleExist = false;

		if ((oddCount == 0 || oddCount == 2) && dsuFlag) {
			return ans;
		}
		else return 0;
	}


	bool checkBridge(int n1, int n2) {
		Graph graph;
		graph.setStates('L', isOriented, isWeighed, vertsAmount, edgesAmount);
		DSU dsu(vertsAmount);
		transformToAdjList();
		for (int i = 0; i < vertsAmount; i++) {
			graph.adjList[i] = adjList[i];
		}
		graph.removeEdge(n1, n2);
		for (int i = 0; i < vertsAmount; i++) {
			int a = i + 1;
			for (auto j = graph.adjList[i].begin(); j != graph.adjList[i].end(); j++) {
				int b = j->first;
				if (dsu.find(a) != dsu.find(b)) {
					dsu.unite(a, b);
				}
			}
		}

		return !dsu.checkConnectivity();
	}

	vector <int> getEuleranTourFleri() {
		Graph graph;
		graph.setStates('E', isOriented, isWeighed, vertsAmount, edgesAmount);
		vector <int> ans(edgesAmount + 1);
		vector <int> power(vertsAmount + 1, 0);
		transformTolistOfEdges();
		for (int i = 0; i < edgesAmount; i++) {
			power[get<0>(listOfEdges[i])]++;
			power[get<1>(listOfEdges[i])]++;
			get<0>(graph.listOfEdges[i]) = get<0>(listOfEdges[i]);
			get<1>(graph.listOfEdges[i]) = get<1>(listOfEdges[i]);
			get<2>(graph.listOfEdges[i]) = get<2>(listOfEdges[i]);
		}
		bool circleFlag;
		int start = checkEuler(circleFlag);
		ans[0] = start;
		transformToAdjList();
		graph.transformToAdjList();
		int curVrtx = start;
		for (int i = 0; i < edgesAmount; i++) {
			bool stepFlag = false;
			for (auto j = graph.adjList[curVrtx - 1].begin(); j != graph.adjList[curVrtx - 1].end(); j++) {
				int neiVrtx = j->first;
				if (!graph.checkBridge(curVrtx, neiVrtx) || power[curVrtx] == 1) {
					stepFlag = true;
					power[curVrtx]--;
					graph.removeEdge(curVrtx, neiVrtx);
					curVrtx = neiVrtx;
					ans[i + 1] = curVrtx;
					power[neiVrtx]--;
					break;
				}
			}
			if (!stepFlag) for (auto j = graph.adjList[curVrtx - 1].begin(); j != graph.adjList[curVrtx - 1].end(); j++) {
				int neiVrtx = j->first;
				if (!graph.checkBridge(curVrtx, neiVrtx) || power[neiVrtx] != 1) {
					power[curVrtx]--;
					graph.removeEdge(curVrtx, neiVrtx);
					curVrtx = neiVrtx;
					ans[i + 1] = curVrtx;
					power[neiVrtx]--;
					break;
				}
			}
			stepFlag = false;
		}
		return ans;
	}

	vector <int> getEuleranTourEffective() {
		Graph graph;
		graph.setStates('E', isOriented, isWeighed, vertsAmount, edgesAmount);
		stack <int> path;
		vector <int> ans(edgesAmount + 1);
		vector <int> power(vertsAmount + 1, 0);
		transformTolistOfEdges();
		for (int i = 0; i < edgesAmount; i++) {
			power[get<0>(listOfEdges[i])]++;
			power[get<1>(listOfEdges[i])]++;
			get<0>(graph.listOfEdges[i]) = get<0>(listOfEdges[i]);
			get<1>(graph.listOfEdges[i]) = get<1>(listOfEdges[i]);
			get<2>(graph.listOfEdges[i]) = get<2>(listOfEdges[i]);
		}
		bool circleFlag;
		int start = checkEuler(circleFlag);
		ans[edgesAmount] = start;
		int ansIt = 0;
		transformToAdjList();
		graph.transformToAdjList();
		int curVrtx = start;
		for (int i = 0; i <= edgesAmount; i++) {
			for (auto j = graph.adjList[curVrtx - 1].begin(); j != graph.adjList[curVrtx - 1].end(); j++) {
				int neiVrtx = j->first;
				path.push(neiVrtx);
				power[curVrtx]--;
				graph.removeEdge(curVrtx, neiVrtx);
				curVrtx = neiVrtx;
				power[neiVrtx]--;
				break;
			}
			if (graph.adjList[curVrtx - 1].size() == 0 && !path.empty()) {
				while (graph.adjList[path.top() - 1].size() == 0) {
					ans[ansIt] = path.top();
					path.pop();
					ansIt++;
					if (path.empty()) {
						break;
					}
					curVrtx = path.top();
				}
			}
		}
		while (!path.empty()) {
			ans[ansIt] = path.top();
			path.pop();
			ansIt++;
		}
		return ans;
	}


	//лаб4

	bool checkBipart(vector <char> &marks)
	{
		queue <int> qVrtx;
		set <int> used;
		for (int i = 1; i <= vertsAmount; i++) used.insert(i);
		qVrtx.push(1);
		marks[1] = 'A';
		int kMarks = 1;
		transformToAdjList();
		while (!qVrtx.empty())
		{
			int curVrtx = qVrtx.front() - 1;
			//char workMark;
			qVrtx.pop();
			for (auto i = adjList[curVrtx].begin(); i != adjList[curVrtx].end(); i++)
			{
				if (marks[i->first] != 'A' && marks[i->first] != 'B')
				{
					marks[i->first] = marks[curVrtx + 1] == 'A' ? 'B' : 'A';
					qVrtx.push(i->first);
					kMarks++;
				}
				else if (marks[i->first] == marks[curVrtx + 1]) return false;
			}
			used.erase(curVrtx + 1);
			if (qVrtx.empty() && !used.empty()) qVrtx.push(*used.begin());
		}
		return true;
	}

	bool bipartDFS(int j, vector <bool> &used, vector <int> &curBipart, vector <bool> &visited)
	{
		if (used[j]) return false;
		used[j] = true;
		for (auto i = adjList[j - 1].begin(); i != adjList[j - 1].end(); i++)
		{
			int to = i->first;
			if ((curBipart[to] == -1) || bipartDFS(curBipart[to], used, curBipart, visited))
			{
				curBipart[to] = j;
				return true;
			}
		}
		return false;
	}

	vector <pair <int, int> > getMaximumMatchingBipart()
	{
		vector <int> curBipart(vertsAmount+ 1, -1);
		vector <pair <int, int> > ans;
		vector <char> marks(vertsAmount + 1, 'N');
		vector <bool> visited(vertsAmount + 1, false);
		vector <bool> used(vertsAmount + 1, false);
		transformToAdjList();
		for (int i = 1; i <= vertsAmount; i++)
		{
			if (marks[i] == 'B') continue;
			for (auto j = adjList[i - 1].begin(); j != adjList[i - 1].end(); j++)
			{
				if (curBipart[j->first] == -1)
				{
					curBipart[j->first] = i;
					visited[i] = true;
					break;
				}
			}
		}
		if (checkBipart(marks))
		{
			for (int j = 1; j <= vertsAmount; j++)
			{
				if (visited[j] || marks[j] == 'B')
				{
					used.assign(vertsAmount + 1, false);
					bipartDFS(j, used, curBipart, visited);
				}
			}
		}
		for (int i = 1; i <= vertsAmount; i++)
		{
			if (curBipart[i] != -1 && curBipart[curBipart[i]] == i && i < curBipart[i]) ans.push_back(make_pair(i, curBipart[i]));
		}
		return ans;
	}

};